use anyhow::Result;
use structopt::StructOpt;

mod cli;
use cli::Opt;

fn init_logger(quiet: bool, verbosity: usize) -> Result<(), log::SetLoggerError> {
    stderrlog::new()
        .module(module_path!())
        .quiet(quiet)
        .verbosity(verbosity)
        .timestamp(stderrlog::Timestamp::Off)
        .init()
}

fn main() -> Result<()> {
    let cli = Opt::from_args();
    init_logger(cli.quiet, cli.verbose)?;

    log::info!("info message");
    Ok(())
}
