use std::fs::create_dir_all;

use structopt::{clap::Shell, StructOpt};

const APP_NAME: &str = "rust-cli";

use std::env;

#[allow(dead_code)]
#[path = "src/bin/app/cli.rs"]
mod app;
use app::Opt;

fn main() {
    let outdir = env::var_os("OUT_DIR").unwrap();
    create_dir_all(&outdir).unwrap();

    let mut app = Opt::clap();
    app.gen_completions(APP_NAME, Shell::Bash, &outdir);
    app.gen_completions(APP_NAME, Shell::Zsh, &outdir);
    app.gen_completions(APP_NAME, Shell::Fish, &outdir);
}
